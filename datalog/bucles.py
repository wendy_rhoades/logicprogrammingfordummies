from pyDatalog import pyDatalog
import math



# Terms
pyDatalog.create_terms('X,Y,Z')
def buclesTest():
    print("Give me all the X so that X is in the range 0..4")
    print(X.in_((0, 1, 2, 3, 4)))
    print()

    print("The result of a query is a set of its possible solutions, in "
          "random order. Each solution has 1 value for each variable in the query."
          "The .data attribute gives access to the result.")
    print(X.in_(range(5)).data)
    print()
    print(X.in_(range(5)) == set([(0,), (1,), (2,), (3,), (4,)]))

    print("Similarly, after a query, a variable contains a tuple of all its possible values."
          "They can be accessed with these methods :")
    print("Data : ", X.data)
    print("First value : ", X.v())
    print("below, '>=' is a variable extraction operator")
    print("Extraction of first value of X: ", X.in_(range(5)) >= X)



buclesTest()


