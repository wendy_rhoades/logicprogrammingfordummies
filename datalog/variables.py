from pyDatalog import pyDatalog
import math



# Terms
pyDatalog.create_terms('X, Y')
pyDatalog.create_terms('twice')
pyDatalog.create_terms('math')

def variablesTest():
    print(X == 1)
    print()

    print("Give me all the X and Y so that X is True and Y is False")
    print((X == True) & (Y == False))
    print()

    print("Give me all the X that are both True and False")
    print((X == True) & (X == False))
    print()

    print("Give me all the X and Y so that X is a name and Y is 'Hello ' followed by the first letter of X")
    print((X == 'diego') & (Y == 'Hello ' + X[0]))
    print()

    print("Give me all the X and Y so that Y is 1 and Y is X+1")
    print((Y == 1) & (Y == X + 1))
    print()

    print(
        "Variables can also represent (nested) tuples, which can participate in an expression and be sliced (0-based).")
    print((X == (1, 2) + (3,)) & (Y == X[2]))
    print()


def twice(a):
    return a + a


def funcionesTest():
    print("To use your own functions in logic expressions, define them in Python, then ask pyDatalog to create logical terms for them:")
    print((X == 1) & (Y == twice(X)))
    print()

    print("Similarly, pyDatalog variables can be passed to functions in the Python standard library:")
    print("Give me all the X and Y so that X is 2 and Y is the square root of X")
    print((X==2) & (Y==math.sqrt(X)))


funcionesTest()


