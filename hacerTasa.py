import random

"""
[cantidad BID, precio BID, cantidad ASK, precio ASK]

"""
CI = [
    [random.randint(1, 5), random.randint(1, 9), random.randint(1, 5), random.randint(1, 9)],
    [random.randint(1, 5), random.randint(1, 9), random.randint(1, 5), random.randint(1, 9)]
]

HS24 = [
    [random.randint(1, 5), random.randint(1, 9), random.randint(1, 5), random.randint(1, 9)],
    [random.randint(1, 5), random.randint(1, 9), random.randint(1, 5), random.randint(1, 9)]
]

HS48 = [
    [random.randint(1, 5), random.randint(1, 9), random.randint(1, 5), random.randint(1, 9)],
    [random.randint(1, 5), random.randint(1, 9), random.randint(1, 5), random.randint(1, 9)]
]

# trae la columna col de una matriz
def getCol(matriz, col):
    return [row[col] for row in matriz]


"""
Enfrenta mercados, ejemplo CI contra 24hs
- toma precio punta vendedora, precioASK
- toma precio punta compradora, precioBID
- hace la diferencia y la compara contra el spread minimoValido
 
"""
def haySpread(bid, colBID, ask, colASK, spread):
    precioBID = bid[0][colBID]
    precioASK = ask[0][colASK]

    diff = precioASK - precioBID
    print("precioBID:", precioBID, "precioASK", precioASK)
    print("diff:", diff)

    if diff > spread:
        return diff
    else:
        return 0


"""
Enfrenta mercados, ejemplo CI contra 24hs
- toma ultima cantidad operada de la punta vendedora
- toma ultima cantidad operada de la punta compradora
- busca la cantidad minima entre ambas y compra contra mercadominimoValido
"""
def hayMercado(bid, colBid, ask, colAsk, mercadoMinimo):
    cantBid = bid[0][colBid]
    cantAsk = ask[0][colAsk]

    mm = min(set([cantBid, cantAsk]))
    print("cantBID:", cantBid, "cantASK:", cantAsk)
    print("Minimo:", mm)

    if mm > mercadoMinimo:
        return mm
    else:
        return 0


# hacer tasa
print("CI:", CI)
print("24HS:", HS24)

spreadMinimo = 1  # rule
colPrecioBid = 1
colPrecioAsk = 3
spread24HS = haySpread(CI, colPrecioBid, HS24, colPrecioAsk, spreadMinimo)
print("Hay spread positivo:", spread24HS)
print()

mercadoMinimo = 2  # rule
colCantBid = 0
colCantAsk = 2
mercado24HS = hayMercado(CI, colCantBid, HS24, colCantAsk, mercadoMinimo)
print("Hay mercado positivo:", mercado24HS)
print()





